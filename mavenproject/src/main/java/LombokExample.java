import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.io.File;
import java.io.FileReader;

// Lombok General Annotations:
// @Setter -> генерирует сеттер для каждого поля
// @Getter -> генерирует геттер для каждого поля
// @EqualsAndHashCode -> генерирует equals + hashCode в зависимости от полей
// @AllArgsConstructor -> генерирует конструктор из всех полей
// @NoArgsConstructor -> генерирует конструктор пустой
// @RequiredArgsConstructor -> генерирует конструктор для final полей
// @ToString -> генерирует toString() в зависимости от полей
// @SneakyThrows -> прокидывает в сигнатуру метода все checked exception'ы
// @Data -> @Getter + @Setter + @RequiredArgsConstructor + @ToString + @EqualsAndHashCode
@Data
@RequiredArgsConstructor
public class LombokExample {
    private int age1;
    private int age2;
    private final int age3;
    private int age4;
    private final int age5;

    public LombokExample(int age1, int age2, int age3, int age5) {
        this.age1 = age1;
        this.age2 = age2;
        this.age3 = age3;
        this.age5 = age5;
    }

    // в value мы можем передать массив/одно исключение, которое "пробрасываем" в сигнатуру метода
    @SneakyThrows
    public void blabla() {
        // throws | try catch
        File f = new File("123123");
        FileReader fr = new FileReader(f);
        Thread.sleep(1000);
        BlablaQ blablaQ = new BlablaQ("Sergey", 20);
    }
}
class TestLombok {
    public static void main(String[] args) {
        LombokExample lombokExample = new LombokExample(1, 2);
        System.out.println(lombokExample);
    }
}

record BlablaQ(String name, int age) {}
