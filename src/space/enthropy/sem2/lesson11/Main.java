package space.enthropy.sem2.lesson11;

import space.enthropy.sem2.lesson10.Binocla;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    /*
    Рефлексия - механизм самоисследования кода
    Что позволяет:
    - Получать информацию о переменных, методах, конструкторах, самом классе...
    - Получать новый экземпляр класса
    - Получать доступ к приватным методам/полям; взаимодействовать с ними
    - Делать все вышеперечисленное в Runtime
    ------
    Минусы:
    - Производительность ухудшается
    - Ограничение безопасности. SecurityManager -> ограничивает рефлексию
    - Нарушаем всевозможные принципы (инкапсуляция)
    - Механизм рефлексии "не приветствуется" при нативной сборке.
     */
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        // Class
        // Field
        // Method
        // Constructor
        // Annotation...
        // 1)
        try {
            Class<?> cl1 = Class.forName("space.enthropy.sem2.lesson11.PersonBinocla");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        // 2)
        PersonBinocla personBinocla = new PersonBinocla("Binocla");
        Class<? extends PersonBinocla> cl2 = personBinocla.getClass();

        // 3)
        Class<PersonBinocla> cl3 = PersonBinocla.class;

//        Method[] methods = cl3.getMethods(); // только public методы (включая и родителя)
//        for (Method method : methods) {
//            System.out.println(method);
//        }
        Method[] methods = cl3.getDeclaredMethods(); // все методы текущего класса
        for (Method method : methods) {
            // System.out.println(method);
            if (method.getName().equals("setName")) {
                method.setAccessible(true);
                method.invoke(personBinocla, "Sergey"); // .invoke принимает первым параметром объект у которого вызываем метод, вторым параметром - массив из передаваемых параметров (опционально)
            }
        }
        for (Method method : methods) {
            // System.out.println(method);
            if (method.getName().equals("getName")) {
                Binocla b = method.getAnnotation(Binocla.class);
                System.out.println(b.qwe());
                System.out.println(b.dsa());
                System.out.println(b.value());
                System.out.println(method.invoke(personBinocla)); // .invoke принимает первым параметром объект у которого вызываем метод, вторым параметром - массив из передаваемых параметров (опционально)
            }
        }
        System.out.println("------");
        Constructor[] constructors = cl3.getConstructors(); // только public конструкторы (см. выше)
        for (Constructor constructor : constructors) {
            System.out.println(constructor);
        }
        System.out.println("-------");
        Binocla b = cl3.getAnnotation(Binocla.class);
        System.out.println(b.dsa());
        System.out.println(b.value());
        System.out.println(b.qwe());

    }
}

@Binocla(value = "Gotcha!")
class PersonBinocla {
    String name;
    int age;

    public PersonBinocla() {
    }

    PersonBinocla(String name) {
        this.name = name;
    }

    public PersonBinocla(String name, int age) {
        this.name = name;
        this.age = age;
    }
    @Binocla(value = "Method", qwe = "get", dsa = "Name")
    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    protected void sayHello() {
        System.out.println("Hello, " + name);
    }
    void sayHelloDefault() {
        System.out.println("Hello, " + name);
    }
    private void sayHelloPrivate() {
        System.out.println("Hello, " + name);
    }
}
