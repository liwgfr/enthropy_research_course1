package space.enthropy.sem2.lesson9;

import org.junit.jupiter.api.*;

public class PersonTest {
    /*
    JUnit 5
    @BeforeAll -> метод static, запускается перед всеми остальными методами
    @BeforeEach -> метод будет запускаться перед каждым тест-кейсом (тестовым методом)
    @AfterAll -> метод static, запускается в конце всех методов
    @AfterEach -> метод будет запускаться после каждого тест-кейса
    @Test -> помечает, что этот метод - тест-кейс
    -------
    @DisplayName -> изменить имя для тест-кейса
    @Disable -> пропускает данный тест-кейс
    @Nested, @Tag ... etc.
    ------
    Assertions -> класс, в котором есть static методы. Предназначен для проверки ожидаемого результата
    Assertions.assertEquals(*expected*, *actual*).
    Assertions.assertNotEquals(*expected*, *actual*).
    Assertion.assertArrayEquals (проверка на равенство массивов)
    Assertions.assertSame (проверка на одинаковость объектов)
    Assertions.assertNull (проверка, что объект null)
    Assertions.assertNotNull (проверка, что объект не null)
    -----
    Assumptions -> класс, static методы. Предназначен для предположения, что у нас, к примеру есть *blabla* переменная окружения


     */
    static Person person;
    static Calculator calculator;

    @BeforeAll
    static void init() {
        System.out.println("Creating Person!");
        person = new Person("Binocla", 20);
        calculator = new Calculator();
    }

    @Test
    void testGetName() {
        Assertions.assertEquals("Binocl", person.getName());
    }

    @Test
    void testVeryVeryComplexSum() {
        Assertions.assertEquals(5, calculator.veryVeryComplexSum(1, 1));
    }
    // TDD - Test Driven Development: сначала пишем тесты - потом код
    // BDD - Behavior Driven Development: сначал описываем поведение - потом код
    // Mock-тестирование: тестирование правильности выполнения кода на копиях
//    @BeforeEach
//    void blabla() {
//        System.out.println("BeforeEach triggered");
//    }

//    @AfterAll
//    static void end(){
//        System.out.println("AfterAll triggered");
//    }

//    @AfterEach
//    void blablabla() {
//        System.out.println("AfterEach triggered");
//    }

//    @Test
//    void testGetAge() {
//        Assertions.assertEquals(20, person.getAge());
//    }

}
