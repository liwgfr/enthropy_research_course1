package space.enthropy.sem2.lesson9;

// Testing
public class Main {
    public static void main(String[] args) {

    }
}

class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

class Calculator {
    public int veryVeryComplexSum(int x, int y) {
        return (int) ((Math.pow(x, 3) + Math.sqrt(y) * 12 + (3 - x)) / 3);
    }
}
