package space.enthropy.sem2.lesson10;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// Annotations
// @Binocla(value = "Sergey")
public class Main {
    @Binocla(value = "Qwqeqwe")
    public static void main(String[] args) {
        @Binocla(value = "50", qwe = "No Way") int a = 10;
        // Аннотация = метаданные. Аннотации используются для анализа кода,
        // учета их при компиляции или во время выполнения.
        // Что можно аннотировать? Классы, методы, поля (field),
        // переменные (local_variable), конструкторы...
        // Аннотация по своей сути это интерфейс (с некоторыми оговорками)
        // Аннотации всегда начинаются с @ (Пример, @Override)
    }
}


// По дефолту у аннотации Target параметр ElementType.TYPE - применима ко всему
// По дефолту у аннотации Retentintion параметр RetentionPolicy.CLASS - остается в .class файлах
