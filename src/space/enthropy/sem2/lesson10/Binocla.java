package space.enthropy.sem2.lesson10;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// "Настройки" для аннотаций (видимость аннотации - область применения)
// и сохраняется ли аннотация (где мы можем увидеть, что она есть и получить к ней доступ)
@Target({ElementType.LOCAL_VARIABLE, ElementType.METHOD, ElementType.TYPE})
// RetentionPolicy.CLASS - Аннотация будет зафиксирована в .class файлах
// RetentionPolicy.RUNTIME - доступ к этой аннотации разрешен во время выполнения (например, с помощью механизма Рефлексии)
// RetentionPolicy.SOURCE - Аннотация НЕ будет зафиксирована в .class файлах
@Retention(RetentionPolicy.RUNTIME)
public @interface Binocla {
    String value() default "Blablabla";

    String qwe() default "Qwe";

    String dsa() default "Dsa";
}
