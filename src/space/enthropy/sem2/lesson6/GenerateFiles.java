package space.enthropy.sem2.lesson6;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateFiles {
    public static void main(String[] args) throws FileNotFoundException {

        for (int i = 1; i < 75; i++) {
            PrintWriter pw = new PrintWriter("files/" + i + ".txt");
            pw.print(generateFile());
            pw.close();
        }

    }

    private static String generateFile() {
        List<Integer> list = new ArrayList<>();
        int a = new Random().nextInt(10000) + 100;
        for (int i = 0; i < a; i++) {
            list.add(new Random().nextInt(750000) - 250000);
        }
        return list.toString().replaceAll("[\\[\\]]|,", "");
    }
}
