package space.enthropy.sem2.lesson6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class General {
    public static void main(String[] args) throws FileNotFoundException {
        List<Long> resultsInTime = new ArrayList<>();
        PS p = new PS();
        for (int i = 1; i < 75; i++) {
            File f = new File("files/" + i + ".txt");
            resultsInTime.add(p.trigger(f));
        }
        System.out.println("Замеры времени в ms: " + resultsInTime.toString().replaceAll("[\\[\\]]|,", ""));
    }
}
