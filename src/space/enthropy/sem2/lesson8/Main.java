package space.enthropy.sem2.lesson8;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        final Shop shop = new Shop();
        Thread t1 = new Thread(() -> {
            try {
                shop.produce();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                shop.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();

    }

    private static class Shop {
        private final List<Integer> list = new ArrayList<>();
        private static final int CAPACITY = 10;

        public void produce() throws InterruptedException {
            int value = 0;
            while (true) {
                synchronized (this) {
                    while (list.size() == CAPACITY) {
                        wait();
                    }
                    System.out.println("Йоу, нам пришел новый комп: " + value);
                    list.add(value++);
                    notify();
                    Thread.sleep(1000);
                }
            }
        }

        public void consume() throws InterruptedException {
            while (true) {
                synchronized (this) {
                    while (list.size() == 0) {
                        wait();
                    }
                    int val = list.remove(0);
                    System.out.println("Покупатель взял этот комп: " + val);
                    notify();
                    Thread.sleep(1000);
                }
            }
        }
    }
}

class BinoclaThread {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            System.out.println("Hello");
            for (int i = 0; i < 100; i++) {

                System.out.println(Thread.currentThread().isInterrupted());
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("Interrupted here");
                    try {
                        throw new InterruptedException("Прерван");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(i);
            }
        });
        thread.start();
        Thread.sleep(5);
        thread.interrupt();
    }


}