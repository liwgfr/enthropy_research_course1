package space.enthropy.sem2.lesson12;

import lombok.SneakyThrows;

import java.io.*;
import java.lang.reflect.Field;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {

        Animal animal = Animal.class.getConstructor().newInstance();
        System.out.println(animal);
        Class<? extends Animal> clazz = animal.getClass();
        Field field = clazz.getDeclaredField("PHONE");
        field.setAccessible(true);
        field.set(animal, 322);
        System.out.println(animal);

        Calc calc = Integer::sum;
        calc.asd(1, 2);
        calc = (x, y) -> x * y;
        calc.asd(2, 3);
        calc = (x, y) -> x / y;
        calc.asd(2, 3);
        calc = (x, y) -> x % y;
        calc.asd(2, 3);
        calc = (x, y) -> x + y * 2;
        calc.asd(2, 3);
        calc = (x, y) -> x + y / 3;
        calc.asd(2, 3);
        calc = (x, y) -> x + y - 123;
        calc.asd(2, 3);


        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("src/space/enthropy/sem2/lesson12/animal.txt"));
        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("src/space/enthropy/sem2/lesson12/animal.txt"));
        outputStream.writeObject(animal);
        outputStream.close();
        Animal recoveredAnimal = (Animal) inputStream.readObject();
        System.out.println("Recovered Animal: " + recoveredAnimal);


    }
}

class Animal implements Serializable {
    private static final long serialVersionUID = 6760520779540313629L;

    private String name;
    private transient int age = 10;
    private final int PHONE = 900; // In Java 12+ final cannot be removed

    public Animal(String name) {
        this.name = name;
    }

    public Animal(int age) {
        this.age = age;
    }

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public Animal() {
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", PHONE=" + PHONE +
                '}';
    }
}

interface Calc {
    int asd(int x, int y);
}
