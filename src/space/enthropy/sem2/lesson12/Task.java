package space.enthropy.sem2.lesson12;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Task {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(in.nextInt());
        }
        list.sort(Comparator.naturalOrder());
        int middle = n % 2 == 0 ? n / 2 : n / 2 + 1;
        list = list.subList(middle, list.size());
        System.out.println(list.stream().mapToInt(x -> x).sum());
    }
}
